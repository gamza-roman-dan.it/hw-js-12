"use strict"

/*

1-
яб повісивби прослуховувач на "window" а саме "keydown" й виводивби у консоль "event.code" або просто "event" там буде більш детальна інформація;

2-
Властивість "key" дозволяє отримати проосто символ, а властивість "code" дозволяє отримати фізичний код клавіші.

3-
keypress - is deprecated застарілий метод й він поєднував у собі два методи які наведені нижче
keydown - це натискання на клавішу й до того її ще можна затримати
keyup - це відпускання клавіши


-------------------------------------------------------------------------------------------------------------------------------------------
*/

//code
const buttonAll = document.querySelectorAll(".btn");
window.addEventListener("keydown", (event) => {
    const clasBlue = document.querySelector(".blue");
    if (clasBlue){
        clasBlue.classList.remove("blue");
    }

    buttonAll.forEach(el => {
        if(event.key === el.innerText){
            el.classList.add("blue");
        }else if(event.key.toUpperCase() === el.innerText ){
            el.classList.add("blue");
        }
    });
})
